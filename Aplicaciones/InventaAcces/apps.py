from django.apps import AppConfig


class InventaaccesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'InventaAcces'
